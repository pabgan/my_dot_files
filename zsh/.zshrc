# To debug ZSH configuration
# o_verbose=0

#########################################################
## SHELL CONFIGURATION
#######################
# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Options
setopt AUTO_CD
setopt AUTO_PUSHD
#setopt MENU_COMPLETE
setopt histignorealldups
# stop backward-kill-word on directory delimiter [13]
autoload -U select-word-style
select-word-style bash

# Use modern completion system [12]
zstyle ':completion:*' completer _complete
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'
autoload -Uz compinit
compinit

## FZF-TAB
# fzf-tab needs to be loaded after `compinit`, but before plugins which will
# wrap widgets, such as
# [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) or
# [fast-syntax-highlighting](https://github.com/zdharma-continuum/fast-syntax-highlighting)!!
source ~/repos/fzf-tab/fzf-tab.zsh

## HISTORY
# Change the command execution time stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"
# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# User configuration
# Do not share history between opened zsh instances
setopt nosharehistory

# Activate syntax highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## FIXES
# Turn off screen freezing [6]
stty ixoff -ixon


#########################################################
## PROMPT
##########
# Set up the prompt
autoload -Uz promptinit

promptinit
prompt adam1

## GIT info in prompt [11]
autoload -Uz vcs_info
precmd() { vcs_info }
# Enable checking for (un)staged changes, enabling use of %u and %c
zstyle ':vcs_info:*' check-for-changes true
# Set custom strings for an unstaged vcs repo changes (*) and staged changes (+)
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
zstyle ':vcs_info:*' color 9dd6f9
# Set the format of the Git information for vcs_info
zstyle ':vcs_info:git:*' formats       '(%b%u%c)'
zstyle ':vcs_info:git:*' actionformats '(%b|%a%u%c)'

# Edit line with C-x C-e
autoload -z edit-command-line
zle -N edit-command-line
bindkey '^Xe' edit-command-line
bindkey '^X^e' edit-command-line

##########################################################
## ENVIRONMENT CONFIGURATION
#############################
export PATH="$PATH:$HOME/.local/bin"
for d in $( ls -d $HOME/.local/bin/* ); do
	    PATH+=":$d"
done
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

export EDITOR='nvim'
export PAGER='less'
### less [1]
export LESS='-R -C -M -I -j 10 -# 4'

### Change default permissions for new directories and files [9]
umask 002

export JAVA_HOME=/usr/lib/jvm/jre
export PATH=$JAVA_HOME:$PATH
 
#########################################################
## HANDLE FILES
################
alias e=$EDITOR
alias em='$EDITOR /tmp/mail.md'
alias v='view'
alias bat='bat --paging=never --style=plain'
alias batp='bat --paging=always --style=full'

alias ls='ls --human-readable --color=auto'
alias  l='ls'
alias ll='ls -l'
alias la='ls -l --all'

alias fm='nnn -d'

alias extract='patool extract'

# Backup files
alias mv='mv -b' # create a backup if file exists
cpb() {
	cp --force --backup=numbered $1 $1
}

take() {
	mkdir -p "$1"; cd "$1"
}

# Find only regular files
findf() {
	find $1 -type f
}


# Copy files keeping the directory structure
alias rsync_with_path='rsync -RLrvzh'

alias diff='diff --color=auto'

# Update a zip file
alias update_zip='zip -er'

#########################################################
## UTILS
#########
alias ...='cd ../..'
alias ....='cd ../../..'
alias cdtmp='cd $(mktemp -d)'

alias colors='for x in {0..255}; do printf "\x1b[38;5;${x}mcolor%-5i\x1b[0m" $x ; if ! (( ($x + 1 ) % 8 )); then echo ; fi ; done ' # [7]

alias alert='echo -e "\a"'
### Get the weather report
alias weather='curl wttr.in'

### Get ip address
alias ipexternal='curl ipinfo.io/ip'
alias ipinternal='ipconfig getifaddr en0'

# Get the PID of a process [4]
pid2() {
	local i
	for i in /proc/<->/stat
	do
		[[ "$(< $i)" = *\((${(j:|:)~@})\)* ]] && echo $i:h:t
	done
}

alias httpserver='python3 -m http.server'

compdef sshrc=ssh

# Search in history
alias hs='history 0 | grep '


#########################################################
## PROGRAM CONFIGS
###################
alias stow='stow --target $HOME'

unset CONTEXTS
export ZSH_CONFIG_HOME=$XDG_CONFIG_HOME/zsh
# Load enabled customizations
for customization in $ZSH_CONFIG_HOME/customizations/enabled/* ; do
	if [[ ${o_verbose} ]]; then echo ">DEBUG> Loading $customization" >&2; fi
	source $customization
done
# Load enabled plugins
for plugin in $ZSH_CONFIG_HOME/plugins/enabled/* ; do
	if [[ ${o_verbose} ]]; then echo ">DEBUG> Loading $plugin" >&2; fi
	source $plugin
done

export DIARIO_PRINTER='bat --paging=never --style=plain --language=md'
#export BAT_THEME=Dracula


#########################################################
## FINALLY
###########
#
# Always start or attach to default Tmux session if connected through SSH
#if [[ -n $SSH_CONNECTION ]]; then
export TERM=tmux-256color
tmuxstart
#fi


#########################################################
## SOURCES
# [1]:  https://opensource.com/article/18/5/advanced-use-less-text-file-viewer
# [2]:  https://unix.stackexchange.com/questions/410456/zsh-completion-make-sshrc-behave-like-ssh
# [3]:  https://github.com/tobixen/calendar-cli
# [4]:  http://grml.org/zsh/zsh-lovers.html
# [5]:  https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/zsh-navigation-tools
# [6]:  http://xahlee.info/linux/linux_Ctrl-s_freeze_vi.html
# [7]:  https://superuser.com/questions/285381/how-does-the-tmux-color-palette-work
# [9]:  https://geek-university.com/linux/set-the-default-permissions-for-newly-created-files/
# [10]: https://gitlab.gnome.org/GNOME/gnome-settings-daemon/-/issues/6
# [11]: https://salferrarello.com/zsh-git-status-prompt/
# [12]: https://stackoverflow.com/questions/22600259/zsh-autocomplete-from-the-middle-of-filename
# [13]: https://stackoverflow.com/questions/444951/zsh-stop-backward-kill-word-on-directory-delimiter

## # bun completions
## [ -s "/home/pabgan/.local/share/reflex/bun/_bun" ] && source "/home/pabgan/.local/share/reflex/bun/_bun"
## 
## # bun
## export BUN_INSTALL="$HOME/.local/share/reflex/bun"
## export PATH="$BUN_INSTALL/bin:$PATH"

