export CONTEXTS=${CONTEXTS:+${CONTEXTS}:}AXON

AXON_CONFIG_DIR="${XDG_CONFIG_HOME}/axon"
axon_config_file="${AXON_CONFIG_DIR}/config.sh"
source ${axon_config_file}


###############################################################################
# DIARIO
###############################################################################
DIARIO_DIR_AXON=${HOME}/work/org/diario
alias diario_axon="export DIARIO_DIR=${DIARIO_DIR_AXON}; diario.sh"
alias dx=diario_axon
alias dxs='dx show'
alias dxe='dx edit'
alias dxa='dx add'
alias dxf='fm ${DIARIO_DIR_AXON}'
alias dxt='export DIARIO_DIR=${DIARIO_DIR_AXON}; diario_task'


###############################################################################
# TODOs
###############################################################################
TODO_CFG_FILE=${XDG_CONFIG_HOME}/todo/work.cfg
alias tx="todo.sh -a -d ${TODO_CFG_FILE}"
_tx()
{
    alias todo.sh='todo.sh -d ${TODO_CFG_FILE}'
    # This is needed to make aliases work in a non-interactive shell,
    # see https://stackoverflow.com/questions/23258413/expand-aliases-in-non-interactive-shells
    setopt ALIASES
    _todo.sh "$@"
}
compdef _tx tx


# Only the day, not time. Used mainly to schedule tasks
fecha() {
	date --iso-8601 --date=$1
}

###############################################################################
# ENVIRONMENT CONFIGURATION
###############################################################################
export SSHHOME=$XDG_CONFIG_HOME/sshrc

# build
export MAVEN_OPTS="-Xms1024m -Xmx4096m"
alias mvn='JAVA_HOME=/usr/java/jdk1.8.0_201; mvn'

# Oracle
#export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib:$LD_LIBRARY_PATH
export SQLPATH=$XDG_CONFIG_HOME/sqlcl/

# Python
export PYTHONPATH=$HOME/.local/python

export PATCHESPATH=$HOME/repos/AXON/feature_config/patches


###############################################################################
# AXON CONFIGURATION
###############################################################################
# DB
DB_MACHINE=sv-usplan-oradb2.dzsi.net:1521/dslodev

# Alias for very frequent connections
alias se='sshrc $CUSTOMER_ENVIRONMENT'
alias dbc=dbconn.sh

# Alias to allow... Too long to explain
alias _#=''

# Alias to attack DCPC REST API
alias rest_dcpc='rest.sh --verbose --protocol http --port 10461 --endpoint api/v1/dcpc --action POST'

# Edit customer configurations
edit_config() {
	files=$(ls ~/work/releases/.configs/*.cfg | fzf --multi --delimiter / --with-nth=-1)
	if [[ ! -z ${files} ]]; then
		# (z) is for splitting the result of the expansion into words
		# using shell parsing to find the words. Otherwise it opens the
		# editor with a filename that is the whole array
		$EDITOR "${(z)files}"
	fi
}


###############################################################################
# SOURCE CODE MANAGEMENT
###############################################################################
alias checkstyle='mvn clean install -DskipTests -Dfindbugs -Dspotbugs'
alias compile="rentr.sh 'ls **/*.java' 'mvn install -Dskip-unpack -DskipTests'"

diff_ticket() {
	ticket=${1}

	# The commit that appears on top which message contains the ticket ID
	after=$(git log --grep="${ticket}"| grep commit | head -n1 | cut -d' ' -f2)
	# The commit previous to the first commit that contains the ticket ID
	first_commit=$(git log --grep="${ticket}" | grep commit | tail -n1 | cut -d' ' -f2)
	before=$(git log ${first_commit}~1 | head -n1 | cut -d' ' -f2)

	echo "git diff --unified=9999 ${before} ${after}"
	git diff --unified=9999 ${before} ${after}
}

###############################################################################
# CURRENTLY WORKING
###############################################################################
customer_configs_path="${HOME}/work/releases/.configs"
set_working_environment() {
	customer_config_file=$(ls ${customer_configs_path}/*.cfg | fzf --multi --delimiter / --with-nth=-1)

	if [[ -n ${customer_config_file} ]]; then
		if [[ -f ${customer_config_file} ]]; then
			grep 'CUSTOMER_CONFIG' ${axon_config_file} >/dev/null && sed -i --follow-symlinks "s/^export CUSTOMER_CONFIG=.*/export CUSTOMER_CONFIG=${customer_config_file##*/}/g" ${axon_config_file} || echo "export CUSTOMER_CONFIG=demo-a" >> ${axon_config_file}
			exec zsh
		else
			echo ">ERROR> ${customer_config_file} not found" >&2
		fi
	fi
}
populate_customer_variables() {
	customer_config_file=$(ls ${customer_configs_path}/${1} | head -n 1)
	# export variables automatically [3]
	set -o allexport
	source ${customer_config_file}
	set +o allexport
	export CUSTOMER_CVS="${HOME}/repos/AXON/releases/${CUSTOMER}"
	export CUSTOMER_DIR="${HOME}/work/releases/${CUSTOMER}/${CUSTOMER_VERSION/v/}"

	echo ">>>>> Working on ${CUSTOMER_ENVIRONMENT} ${CUSTOMER_VERSION} <<<<<"
	znt_cd_hotlist=( "${znt_cd_hotlist[@]}" '------------------------------------=currently working=-' )
	znt_cd_hotlist=( "${znt_cd_hotlist[@]}" "${CUSTOMER_CVS/$HOME/~}" "${CUSTOMER_DIR/$HOME/~}" )
}
# This $CUSTOMER_CONFIG comes from sourcing $axon_config_file at the top of this file
populate_customer_variables ${CUSTOMER_CONFIG}
alias cdc='cd ${CUSTOMER_DIR}'
alias cdcg='cd ${CUSTOMER_CVS}'



###############################################################################
# ENVIRONMENTS
###############################################################################
# Get version installed on a customers support environment
get_customer_version(){
	for env in ${@}; do
		echo "--- ${env} ---"
		ssh ${env} 'ls -d install/*-*' | sed 's/install\///'
	done
}
compdef get_customer_version=ssh
compdef rsync_to.sh=ssh

###############################################################################
# TASK MANAGEMENT
###############################################################################
#
task_info_specifics() {
	echo ''
	echo 'JIRA status'
	echo '-----'
	ji.py read --format md --issue ${1} --fields 'summary,fixVersions,assignee,status,updated'

}
get_ticket_info() {
	task=${1}
	mkdir -p .jira
	ji.py read --issue ${task} --fields 'summary'     | pandoc --from jira --to markdown | tr '\n' ' ' | tr -d '\\' | sed 's/\s$/\n/' | dos2unix > .jira/title
	ji.py read --issue ${task} --fields 'description' | pandoc --from jira --to markdown | sed 's/\s$/\n/' | dos2unix > .jira/description
	ji.py read --issue ${task} --fields 'unit_test'   | tr -d '\\' | dos2unix > .jira/unit_test
	# raw fixVersions example: \[\'V22.12 Centurylink (ENS and CRIS)\'\]
	ji.py read --issue ${task} --fields 'fixVersions' | tr -d '\\' | dos2unix > .jira/fixVersions
}
task_start_specifics(){
	zparseopts -F -D -verbose=o_verbose -no-assign=o_no_assign
	if [ ! ${o_no_assign} ]; then
		echo ">INFO> Assigning"
		o_assign_to_me='--assign-to-me'
	else
		echo ">INFO> Skipping assigning"
	fi

	get_ticket_info ${TASK}

	# About where to check it
	echo -e "Verified in **${CUSTOMER_ENVIRONMENT} ${CUSTOMER_VERSION}**.\n" >> ticket.md

	# echo -e "To be verified in:\n" >> ticket.md
	fix_versions=$(cat .jira/fixVersions | tr -d ",[]")

	# (z) Split the result of the expansion into words using shell parsing
	# to find the words, i.e.  taking into account any quoting in the
	# value.
	for fv in ${(z)fix_versions}; do
		# Remove the json list formatting characters
		fv=${fv//\'/}
##		echo "- [pending] ${fv//\'/}" >> ticket.md

		# Remove the version part
		customer=${fv%% *}
		# Make it lowercase
		customer=${(L)customer}
		# Convert expresse into demo
		customer=${customer/expresse/demo} 

		# Remove the customer name part
		version=${fv##* }
		# Make it lowercase
		version=${(L)version}
		# If version was 'next version' the modifications left it as 'version'
		# so remove it to not add a version to the task
		version=${version/version/}

		# Add a task to follow up on what is it that I am doing
		tx add "+$TASK '$(cat .jira/title)' en +${customer} ${version:++${version}} "
	done
	# Mark as currently working on the task that points to version in works right now
	task_current=$(tx ls $TASK +${CUSTOMER} ${CUSTOMER_VERSION%.*} | head -n 1)
	tx pri ${task_current%% *} a

	# Rutinary checks
	module_fix_for_version=$(ji.py read --issue $TASK --fields 'module_fix_for_version' | grep -oE "[^ ]+[ |-]v?[0-9|.]+")

	find_template='find install/ installAdditional/ -name "*${module_name}*.jar"'
	if [ ! -z $module_fix_for_version ]; then
		echo -e "\n##### Modules versions\n" >> ticket.md

		for mfv in ${(f)module_fix_for_version}; do
			echo -e "- [<++>] $mfv\n" >> ticket.md

			echo '```sh' >> ticket.md

			# We remove the version part
			module_name=${mfv% *}
			echo "$ ${(e)find_template}" >> ticket.md
			ssh $CUSTOMER_ENVIRONMENT "${(e)find_template}" >> ticket.md

			echo -e '```\n' >> ticket.md
		done
	fi

	# Update Jira
	ji.py update --issue $TASK $o_assign_to_me
}

task_resume_specifics(){
	#jira_ticket $TASK
}

task_close_specifics() {
	#get_closed_tickets.sh &
}


jira_ticket(){
	xdg-open "https://assia-inc.atlassian.net/browse/$1"
}

task_package_for_jira() {
	package_name="$(basename $(cut -d' ' -f1 <(pwd)))-$CUSTOMER_VERSION"
	tar --dereference -cvzf $package_name.tar.gz --exclude='*.md' --exclude='.*' --exclude='*.sql' --exclude='*.sh' --exclude='*.png' --exclude='backup' --exclude='scrapbook*' --exclude='setup.cfg' (s|t|r)<0-100>
}


###############################################################################
# USEFUL FUNCTIONS for tickets
###############################################################################
# Handle configurations for tests
template_test() {
	template="$1"
	tname="$2"
	to_copy='config/ setup.cfg config.sh config.sql'

	origin_dir=$(pwd)

	mkdir $tname
	(cd $template && cp -r $(echo $to_copy) $origin_dir/$tname)
	cd $tname
}
alias tt='template_test'

# Place configured files in their places
push_config() {
	rsync -Krvh "${1:-./}" --exclude='*.swp' --exclude='*~*' ${2:-$CUSTOMER_ENVIRONMENT}:
}

alias backup='rsync -Rrvzh'
alias mm='module_manager.sh $CUSTOMER_ENVIRONMENT'

mock_server_restart() {
	se '~/installAdditional/mock_server/tomcat/bin/shutdown.sh && ~/installAdditional/mock_server/tomcat/bin/startup.sh'
}

open_ticket_in_browser() {
	task_export_name
	firefox --new-window https://dzsi.atlassian.net/si/jira.issueviews:issue-html/$TASK/$TASK.html
}
alias otb='open_ticket_in_browser'

# Convert ticket files into Jira comment in html
comment_preview() {
	rentr.sh 'find ./ -type f' generate_comment_preview.sh
}
alias cpv=comment_preview

###############################################################################
# UTILS
###############################################################################
# Just throw a fast query 
query() {
	query=${1}

	echo "${query}" | sqlcl ${CUSTOMER_DB}/assia@${DB_MACHINE}
}

###############################################################################
# SHOW STATUS
###############################################################################
#
# Compare versions numbering [4]
verlte() {
    printf '%s\n' "$1" "$2" | sort -C -V
}
verlt() {
    ! verlte "$2" "$1"
}
whatsnext_axon() {
	if [[ $HOSTNAME == 'manoliTo' || $HOSTNAME == 'roBin' ]]; then
		echo "#########################################################"
		echo "# TODO priorities"
		tx | head -n -2
		echo "\n#########################################################"
		echo      "# TODO for $CUSTOMER $CUSTOMER_VERSION"
		tx ls +${CUSTOMER} +${CUSTOMER_VERSION} | head -n -2
		# Versions 23.6 and 23.6.0 should be the same
		if [[ "${CUSTOMER_VERSION}" =~ .*\.0$ ]]; then
		        tx ls +${CUSTOMER} +${CUSTOMER_VERSION%.0} | head -n -2
		fi

		echo "\n~~~~~~~~~~ previous versions for this customer ~~~~~~~~~~"
		versions=($(tx lsprj | grep '+v'))
		for version in $versions ; do
			normalized_version=${version#+v}
			normalized_customer_version=${CUSTOMER_VERSION#v}
			if [[ "${normalized_version}.0" != ${normalized_customer_version} ]]; then
				verlt ${version#+v} ${CUSTOMER_VERSION#v} && tx ls +${CUSTOMER} ${version} | head -n -2
			fi
		done
	fi
}
alias nx='whatsnext_axon'


###############################################################################
# SOURCES
###############################################################################
# [1] https://superuser.com/questions/271986/execute-a-command-from-another-directory-in-bash#271992
# [2] https://stackoverflow.com/questions/44758736/redirect-stderr-to-dev-null
# [3] https://unix.stackexchange.com/questions/79064/how-to-export-variables-from-a-file
# [4] https://stackoverflow.com/questions/4023830/how-to-compare-two-strings-in-dot-separated-version-format-in-bash
