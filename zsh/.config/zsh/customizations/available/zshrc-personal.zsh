export CONTEXTS=${CONTEXTS:+${CONTEXTS}:}personal
if [[ -f  $XDG_CONFIG_HOME/user-dirs.dirs ]]; then
	source $XDG_CONFIG_HOME/user-dirs.dirs
fi

#########################################################
## CONNECTIONS
########
alias ssh_reverse_ganvis='ssh -D 8484 pablo@ganvis.ddns.net -p 4422'
alias ssh_reverse_guaranuzax='ssh -D 8484 pabgan@guaranuzax.ddns.net -p 4422'

#########################################################
## DIARIO
########
DIARIO_DIR_PERSONAL=$HOME/Documents/org/notas
alias diario_personal="export DIARIO_DIR=${DIARIO_DIR_PERSONAL}; diario.sh"
alias dp=diario_personal
alias dps='dp show'
alias dpe='dp edit'
alias dpa='dp add'
alias dpf='dp; fm $DIARIO_DIR'
alias dpt='export DIARIO_DIR=${DIARIO_DIR_PERSONAL}; diario_task'


#########################################################
## TODOS
########
alias tp="todo.sh -a -d $XDG_CONFIG_HOME/todo/personal.cfg"
_tp()
{
    alias todo.sh='todo.sh -d $XDG_CONFIG_HOME/todo/personal.cfg'
    # This is needed to make aliases work in a non-interactive shell,
    # see https://stackoverflow.com/questions/23258413/expand-aliases-in-non-interactive-shells
    setopt ALIASES
    _todo.sh "$@"
}
compdef _tp tp

#########################################################
## BLOG
########
alias blog='export DIARIO_DIR=$HOME/Documents/blog/public; diario.sh'

#########################################################
## CAJA FUERTE
########
cajafuerte_path=~/sync/sensitive/cajafuerte.img
cajafuerte_mountpoint=/mnt/cajafuerte

abretesesamo() {
	documents_directory=${XDG_DOCUMENTS_DIR}
	# TODO: remove following commented line if no errors are seen in a while
	#sudo mkdir -p ${cajafuerte_mountpoint}
	echo ">INFO> Opening vault" >&2
	pass local/archivos-cifrados/cajafuerte.img | sudo cryptsetup open ${cajafuerte_path} cajafuerte
	echo ">INFO> Mounting vault in ${cajafuerte_mountpoint}" >&2
	sudo mount --mkdir /dev/mapper/cajafuerte ${cajafuerte_mountpoint}
	sudo chown pabgan:pabgan ${cajafuerte_mountpoint}

	for directory in ${cajafuerte_mountpoint}/*/; do
		echo ">INFO> Mounting ${directory} in ${documents_directory}"
		sudo mount --mkdir --bind ${directory} ${documents_directory}/$(basename ${directory});
	done
}

cierratesesamo() {
	for directory in $(mount | grep 'cajafuerte' | cut -d' ' -f3); do
		echo ">INFO> Unmounting ${directory}" >&2
		sudo umount ${directory};
	done
	# TODO: remove following two commented lines if no errors are seen in a while
	#echo ">INFO> Unmounting ${cajafuerte_path}" >&2
	#sudo umount ${cajafuerte_mountpoint}
	echo ">INFO> Removing temporary directory ${cajafuerte_mountpoint}" >&2
	sudo rmdir ${cajafuerte_mountpoint}
	echo ">INFO> Closing vault" >&2
	sudo cryptsetup close cajafuerte
}

#########################################################
## SHOW STATUS
###############

whatsnext_personal() {
	if [[ $HOSTNAME == 'manoliTo' || $HOSTNAME == 'roBin' ]]; then
		echo '#########################################################'
		echo '# TODO priorities'
		tp | head -n -2
		echo -e '\n~~~~~~~~~~~~ INBOX ~~~~~~~~~~~~~~~~'
		tp ls -@ -+ | head -n -2
	fi
}
alias np='whatsnext_personal'

#if command -v tp >/dev/null; then tp listpri a; fi


#########################################################
## SHOW STATUS
###############
alias find_code_files="find ~/repos/ -type f                  \
                                     -not -path '*/.git/*'    \
                                     -not -path '*/.repo/*'   \
                                -and -not -path '*/target/*'  \
                                -and -not -name '*.pyc'       \
                                -and -not -name '*.gz'        \
                                -and -not -name '*.old'       \
                                -and -not -name '*.so'       \
                      "
alias edit_code='$EDITOR $(find_code_files | fzf --multi)'
