# move downloaded file here
downloaded-here () {
	find ~/Downloads/ | fzf --multi | xargs -I{} mv "{}" .
}
alias th=downloaded-here
zle -N downloaded-here
bindkey '^hd' downloaded-here

# copy Template Here
template-here () {
	find -L ~/Templates/ -not -path "*/.git/*" -type f | fzf --multi | xargs -I{} cp "{}" .
}
alias th=template-here
zle -N template-here
bindkey '^ht' template-here
 
# move last screenshot here
alias sch='mv "$(find  ~/Pictures/ -name "Screenshot*" -type f | tail -n1)"'

