context() {
	help_message="Syntax is: $(basename $0) enable/disable context_name"

	if [[ $# != 2 ]]; then
		echo "ERROR: invalid syntax." >&2
		echo "$help_message" >&2
		exit 2
	fi
	action=${1}
	context_name=${2}

	# We create the file, just in case it does not exist
	touch ~/.context

	# Now that we are sure the file exist
	# we delete any reference to the context
	# thus disabling it
	sed -i --follow-symlinks "g/$context/d" ~/.context

	# Now, if the user wanted to enable it, we enable it
	if [[ ${action} == 'enable' ]]; then
		echo "export $context=true" >> ~/.context
	fi

	exec zsh
}
