#########################################################
## TMUX CONFIGURATION
#
alias tma='tmux attach -t'
compdef '__tmux-sessions-separately' tma

# Start tmux with 'default' as session name or attach to it if exists
tmuxstart_default() {
	if tmux ls | grep "default" 2> /dev/null ;
	then
		echo "default session found, attaching..."
		tmux attach -t default
		return 0;
		#rest of tmux script to create session named "sess"
	else
		echo "default session not found, opening one..."
		tmux new -s default
	fi
}

tmuxstart() {
	if [ ! "$TMUX" ]; then
		tmuxstart_default
	fi
}

