alias p='pass'
alias pc='pass -c'

# Multi-Factor-Authentication
mfa() {
	oathtool -b --totp "$(pass $1 | head -n1)"
}
compdef _pass mfa
