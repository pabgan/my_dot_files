# switch contexts
realm_is() {
	if [ -f ~/.context ]; then
		sed -i --follow-symlinks "s/^export REALM=.*/export REALM=${1}/g" ~/.context
	else
		echo "export REALM=${1}" > ~/.context
	fi
	exec zsh
}
realm_is_personal() {
	realm_is personal
}

realm_is_work() {
	realm_is work
}

