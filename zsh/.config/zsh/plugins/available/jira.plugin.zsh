export JIRA_URL='https://assia-inc.atlassian.net'
# quickly search in tickets
alias jira_search='ji.py read --format md --query '
alias js='jira_search'
# Translate to Jira and copy to clipboard a comment written in Markdown
export jira_users_list="${XDG_CONFIG_HOME}/jipy/jirausers.cfg"
source ${jira_users_list}

jira_search_ticket_in_current_version() {
	fix_versions=$(cat ${CUSTOMER_DIR}/status/fix-versions.txt)
	# smash all into one line csv like
	fix_versions=$(echo ${fix_versions} | tr '\n' ';')
	# place appropariate separators
	fix_versions=$(echo ${fix_versions} | sed "s/;/', '/")
	# place the starting quote for the first version
	fix_versions=$(echo ${fix_versions} | sed "s/^/'/")
	# place the final quote for the last version
	fix_versions=$(echo ${fix_versions} | sed "s/;$/'/")
	# remove the extra separators
	fix_versions=$(echo ${fix_versions} | sed "s/, '$//")
	echo ">INFO> searching in ${fix_versions}"

	ji.py issues --fields 'summary,updated,assignee,fixVersions,status' "project = DSLE AND fixVersion in (${fix_versions}) and text~'${1}'" ;
}

alias jut='task_export_name; ji.py update --issue ${TASK}'

# Convert ticket files into Jira comment in html
comment_preview() {
	rentr.sh 'find ./ -type f' generate_comment_preview.sh
}
alias cpv=comment_preview

resultcat(){
	cat ticket.md | grep -v '^_#'
	# [2] will leave globbing expressions which don't match anything as-is,
	# and you'll get an error message from the command
	setopt +o nomatch
	# (s)cenario | (t)est | (r)eproduced
	cat ((s|t|r)[[:digit:]])*/**/(scenario|test).md | grep -v '^_#' 2>/dev/null
}


