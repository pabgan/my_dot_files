## Fix stuff that breaks from time to time
# sometimes one monitor has night light but not the other [10]
alias fix_night_light='systemctl --user restart org.gnome.SettingsDaemon.Color.target'

# sometimes clipboard stops storing more than one entry
alias fix_clipboard='gpaste-client empty; gpaste-client restart'

# sometimes I get
# Bad owner or permissions on /home/pganuza/.ssh/config
alias fix_ssh_config_permissions='chmod 644 ~/.ssh/config; chmod 644 ~/.ssh/ssh.d/*'
