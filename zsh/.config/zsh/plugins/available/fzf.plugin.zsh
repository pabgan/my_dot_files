#########################################################
## FZF CONFIGURATION
#
# Setup fzf
# ---------
if [[ ! "$PATH" == */home/pabgan/repos/fzf/bin* ]]; then
	PATH="${PATH:+${PATH}:}/home/pabgan/repos/fzf/bin"
fi

source <(fzf --zsh)

# Auto-completion
# ---------------
# Fedora
[ -f  /home/pabgan/repos/fzf/shell/completion.zsh ] && source /home/pabgan/repos/fzf/shell/completion.zsh
# Debian
[ -f /usr/share/doc/fzf/examples/completion.zsh ] && source /usr/share/doc/fzf/examples/completion.zsh


# Key bindings
# ------------
# Fedora
[ -f  /home/pabgan/repos/fzf/shell/key-bindings.zsh ] && source /home/pabgan/repos/fzf/shell/key-bindings.zsh
# Debian
[ -f  /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh


export FZF_DEFAULT_OPTS="--preview='bat -n --color=always {}' --preview-window=right --bind 'ctrl-p:toggle-preview',ctrl-u:preview-down,ctrl-i:preview-up"

## Exclude target directory from options
## I found these variables in /usr/share/doc/fzf/examples/key-bindings.zsh
#export FZF_CTRL_T_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
#                                        -o \\( -type f -and -not -path '*/target/*' \\) -print \
#                                        -o \\( -type d -and -not -path '*/target/*' \\) -print \
#                                        -o -type l -print 2> /dev/null | cut -b3-"
#
#export FZF_ALT_C_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
#	-o \\( -type d -and -not -path '*/target/*' \\) -print 2> /dev/null | cut -b3-"
#
#export FZF_DEFAULT_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
#                                        -o \\( -type f -and -not -path '*/target/*' \\) -print \
#                                        -o \\( -type d -and -not -path '*/target/*' \\) -print \
#                                        -o -type l -print 2> /dev/null | cut -b3-"
#
#
export FZF_DEFAULT_COMMAND="find -L .      -not -path '*/target/*' \
	-and -not -path '*/.git/*'"

export FZF_CTRL_T_COMMAND="find -L .  -type f \
	-and -not -path '*/target/*' \
	-and -not -path '*/.git/*'"

