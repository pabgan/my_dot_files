# When you however forget that you already are in a ranger shell and start 
# ranger again you end up with ranger running a shell running ranger [1]. To 
# prevent this:
ranger() {
    if [ -z "$RANGER_LEVEL" ]; then
        /usr/bin/ranger "$@"
    else
        exit
    fi
}

#########################################################
## SOURCES
# [1]: https://wiki.archlinux.org/index.php/Ranger
