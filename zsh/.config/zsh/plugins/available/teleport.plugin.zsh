cdd() {
	cd $(dirname "${1}")
}

#########################################################
## FASD CONFIGURATION
#
# Example for a minimal zsh setup (no tab completion):
eval "$(fasd --init zsh-hook)"

# yank file or directory path into clipboard
zle -N fasd_yank_path
fasd_yank_path() {
	text=$(fasd -Ral | fzf --multi)
	echo -n ${text} | xclip
}
bindkey '^zy' fasd_yank_path

# paste file or directory path into terminal
zle -N fasd_paste_path
fasd_paste_path() {
	BUFFER="$BUFFER $(fasd -Ral | fzf --multi)"
	zle .end-of-line
}
bindkey '^zp' fasd_paste_path

# widget to jump to frecent directories
zle -N fasd_change_dir
fasd_change_dir() {
	selection=$(fasd -Rdl | fzf --preview='ls {} --color=always')
	if [[ -n $selection ]]; then
		builtin cd "${selection}" 2>/dev/null || builtin cd $(dirname "${selection}")
	fi
}
bindkey '^zc' fasd_change_dir


# widget to edit frecent files
zle -N fasd_edit_files
fasd_edit_files() {
	selection=$(fasd -Rfl | fzf --multi)
	if [[ -n $selection ]]; then
		$EDITOR $selection
	fi
}
bindkey '^ze' fasd_edit_files

#########################################################
## ALL FILES CONFIGURATION
# locate_files='locate --existing /home/pabgan/ | grep -vE "/home/pabgan/.git"'
# 
# # yank file or directory path into clipboard
# zle -N yank_path
# yank_path() {
# 	text=$(eval ${locate_files} | fzf --multi --delimiter='/' --with-nth='2..')
# 	echo -n ${text} | xclip
# }
# bindkey '^gy' yank_path
# 
# # paste file or directory path into terminal
# zle -N paste_path
# paste_path() {
# 	BUFFER="$BUFFER $(eval ${locate_files} | fzf --multi --delimiter='/' --with-nth='2..')"
# 	zle .end-of-line
# }
# bindkey '^gp' paste_path
# 
# # widget to jump to a directory
# # https://superuser.com/questions/408060/how-to-use-locate-to-search-for-folders-only
# zle -N change_dir
# change_dir() {
# 	selection=$(eval ${locate_files} | fzf --multi --delimiter='/' --with-nth='2..')
# 	if [[ -n ${selection} ]]; then
# 		# Try to cd into selection
# 		#    - if it is a directory it will work
# 		#    - if it is a file it will fail, strip the file name and
# 		#      cd into the directory of the file
# 		cd ${selection} 2> /dev/null || cd $(dirname ${selection})
# 	fi
# }
# bindkey '^gc' change_dir
# 
# 
# # widget to edit files
# # really anything, because I could not get an regexp to filter out directories
# zle -N edit_files
# edit_files() {
# 	selection=$(eval ${locate_files} | fzf --multi --delimiter='/' --with-nth='2..')
# 	if [[ -n $selection ]]; then
# 		$EDITOR $selection
# 	fi
# }
# bindkey '^ge' edit_files
