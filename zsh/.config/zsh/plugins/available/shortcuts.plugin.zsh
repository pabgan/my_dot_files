#########################################################
## GOODIES
###########

# When hitting CTRL+<space> use mimeopen to open a file that is in the command
# line, probably because it was searched with FZF
zle -N open-file-widget
bindkey '^ ' open-file-widget
# Search a file with fzf and then open it in an editor
#alias sc='fzf | xargs $EDITOR'
open-file-widget() {
	BUFFER="mimeopen $BUFFER"
	zle accept-line
}

# Edit command in editor
autoload edit-command-line
zle -N edit-command-line
bindkey '^xe' edit-command-line

# Excute last command with sudo
zle -N sudo-that
bindkey '\E\E' sudo-that
sudo-that() {
	BUFFER="sudo !!"
	zle accept-line
}
