#########################################################
## GIT CONFIGURATION
#
alias g='git'
alias gnp='g --no-pager'
alias gs='g status'
alias gc='g commit'
alias ga='g add'
alias gd='g diff'
alias gnpd='gnp diff'
alias gl='g log --oneline --graph --decorate --all'
alias gp='g push; alert'
alias gcb='g checkout $(git branch | fzf)'


# Visually search for commits and copy the selected ones to clipboard
gcop() {
	shash='%Cred%h'
	author='%Creset%an'
	date='%Cgreen%as'
	message='%Creset%s'
	tab='%x09'
	git --no-pager log \
		--oneline \
		--pretty=format:"${shash} ${author}${tab}${date}${tab}${message}" \
		--color='always' $1 |
	fzf -i -e +s \
		--ansi \
		--multi \
		--preview-window=down,85% \
		--preview="echo {} |
			   grep -o '[a-f0-9]\{7\}' |
			   xargs -I % sh -c 'git show --color=always %'" |
	# copy only the hash
	cut -d' ' -f1 | tr -d '\r\n'| xclip
}
