#########################################################
## TASK MANAGEMENT
#
# TODO: Create one function only that starts task or resumes it
#       depending on whether it finds a directory with that name

task_get_name_from_path() {
	basename $(cut -d' ' -f1 <(pwd))
}

task_export_name() {
	# Extract current directory name
	export TASK=$(task_get_name_from_path)
}

task_info() {
	task_export_name
	task=${1:-$TASK}

	#echo "~~~~~~~~~~~~~~~~~ $task info ~~~~~~~~~~~~~~~~~"
	figlet -m5 $task
	echo 'Notes'
	echo '-----'
	dxs +$task
	echo ''
	echo 'Tasks'
	echo '-----'
	tx -x ls +$task
	task_info_specifics $task
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
}

## task_info_specifics() {
## 	# Nothing to do by default.
## 	# To be overridden by environment specific configurations
## }

task_start() {
	figlet -m5 'iGloria a Dios!'

	if [ -z $1 ]; then
		echo "Usage: task_start task_name [text]"
		return 1;
	fi
	# If it is a url like
	# - https://dzsi.atlassian.net/browse/DSLE-27382
	# - https://dzsi.atlassian.net/issues/DSLE-27364?jql=project%20%3D%20DSLE%20AND%20fixVersion%20%3D%20%22Expresse%20V24.10%22%20and%20fixVersion%3D%20%22Expresse%20V24.8.1%22%20AND%20status%20%3D%20Resolved
	# then take only the last part, that is, the ticket ID in Jira
	task_name=${${1%\?*}##*/}
	text=${2}

	take ${task_name}
	shift
	task_export_name
	TASK_DIR=$(pwd)
	tmux rename-session $TASK
	# next line does not work, but it tries to set the working directory for new panes
	#tmux attach-session -c "#{pane_current_path}"
	#tmux attach-session -c "$pwd"
	task_start_specifics $@
	title=$(cat .jira/title) 2> /dev/null
	dxa "# Comenzando con +$TASK${title+ - $title}${text+"\n$text"}\nEn +${CUSTOMER} +${CUSTOMER_VERSION}"
	echo ''
	task_info $TASK
}

## task_start_specifics() {
## 	# TODO: review if this still makes sense
## 	# Nothing to do by default.
## 	# To be overridden by environment specific configurations
## }

task_resume() {
	task_export_name
	task_resume_specifics
	title=$(cat .jira/title) 2> /dev/null
	dxa "# +$TASK - ${title}\n\nContinuando.${1+"\n\n$1"}"
	if tmux ls | grep "$TASK" &> /dev/null ;
	then
		echo "task session found, attaching..."
		tmux switch-client -t $TASK
	else
		echo "task session not found, opening one..."
		tmux
		tmux rename-session $TASK
	fi
	# next line does not work
	#tmux attach-session -c "#{pane_current_path}"
	echo ''
	TASK_DIR=$(pwd)
	tx -a pri $(t -p ls $TASK | head -n 1 | cut -d' ' -f1) a
	task_info $TASK
}

## task_resume_specifics() {
## 	# TODO: review if this still makes sense
## 	# Nothing to do by default.
## 	# To be overridden by environment specific configurations
## }

task_close() {
	task_export_name
	title=$(cat .jira/title) 2> /dev/null
	dxa "# +$TASK - ${title}\n\nTerminado.${1+"\n\n$1"}"
	task_info $TASK
	task_close_specifics
}

## task_close_specifics() {
## 	# TODO: review if this still makes sense
## 	# Nothing to do by default.
## 	# To be overriden by environment specific configurations
## }

diario_task() {
	task_export_name
	title=$(cat .jira/title) 2> /dev/null
	$EDITOR $(dxa "# +$TASK - ${title}\n\n")
}
