"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VARIABLES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let ROOT_PATH=system('pwd')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" OPTIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enter the current millenium [2]
set nocompatible
"set showcmd         " Show (partial) command in status line.
set hidden           " Hide buffers when they are abandoned
set mouse=           " Disable mouse usage (all modes)
set undofile         " Maintain the undo history even after the file is saved [1]
set wcm=<C-Z>        " Make wildchar work within keybindings

" load plugin manager
"lua require('plugins')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search settings
""""""""""""""""""
set ignorecase       " Do case insensitive matching
set smartcase        " Do smart case matching
set incsearch        " Incremental search
"set autowrite       " Automatically save before commands like :next and :make
set nowrapscan       " Search stops at the end of the buffer (or beginning).
"set backupdir=.local/share/nvim/backup

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Switches
"""""""""""
" Reload config
noremap <leader>R :source ~/.config/nvim/init.vim<CR>

" Underline current line
nmap <leader>sh :set cursorline!<CR>

" Toggle showing line numbers
nmap <leader>sn :set invnumber invrelativenumber<CR>

" Toggle showing a line to know where to wrap the text [3]
nmap <leader>sc :execute "set colorcolumn=" . (&colorcolumn == "" ? "80" : "")<CR>

" Toggle showing metacharacters
nmap <leader>sl :set list!<CR>

" Toggle wrapping lines
nmap <leader>sw :set wrap!<CR>

" Toggle paste
"set nopaste
nmap <leader>sp :set paste!<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" APPEARANCE
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
syntax on
set cursorline       " Show a subtle highlight on cursor's current line for easy location
set cursorcolumn     " Show a subtle highlight on cursor's current column for alignment purposes
set showmatch        " Show matching brackets.
set hlsearch         " Highlight all search matches
set tabstop=4

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
""colorscheme blue
""colorscheme darkblue
"colorscheme default
""colorscheme delek
"colorscheme desert
""colorscheme elflord
""colorscheme evening
"colorscheme habamax
""colorscheme industry
"colorscheme koehler
colorscheme lunaperche
""colorscheme morning
""colorscheme murphy
"colorscheme pablo
""colorscheme peachpuff
""colorscheme quiet
""colorscheme ron
""colorscheme shine
""colorscheme slate
""colorscheme torte
""colorscheme zellner

" set charcoal background
set background=dark
hi Normal guibg=#222021

" Folded lines cannot be comfortably read with default background color
" highlight Folded     ctermbg=17

" Blinking cursor
set guicursor=n:blinkon1,i:blinkon1,i:ver15

" Display a permanent status bar at the bottom of the vi screen showing the
" filename, row number, column number, etc. [1]
set laststatus=2

" For line number to appear in the line selected and all the other ones
" relative to it
set number relativenumber

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FILE SETTINGS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin on
set noexpandtab
filetype on
filetype indent plugin on
"if $CONTEXTS =~ "DZS"
"endif
echo "Contexts are" $CONTEXTS
autocmd FileType markdown setlocal colorcolumn=80

" XML [10] 
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FEATURES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Built in fuzzy search [2]
"""""""""""""""""""""""""""
" Search down into subfolders recursively
" Provides tab-completion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" NOW WECAN:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy
"
" THINGS TO CONSIDER:
" - :b lets you autocomplete any open buffer

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spelling
"""""""""""""""""""""""""""
" Enable spell checking by default
set spelllang=es
nmap <leader>ss :set spell!<CR>
nmap <leader>sss :set spelllang=es<CR>
nmap <leader>sse :set spelllang=en<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Folding
"""""""""""""""""""""""""""
set foldmethod=syntax
set nofoldenable
nmap <leader>sf :set nofoldenable!<CR>

" Adjust column widths
nmap <leader>gq vipgq

" Search for what it is selected pressing / twice
vnoremap // y/<C-R>"<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tags
"""""""""""""""""""""""""""
" Update tag file
autocmd BufWritePost *.java silent !(cd $(dirname $(find_upwards.sh .java.tags)) && rm .java.tags && ctags --exclude='*/target/*' --exclude='*/.git/*' --exclude='*/*-jenkinsh/*' --languages=java -f .java.tags -R * &)
autocmd BufWritePost *.py   silent !(cd $(dirname $(find_upwards.sh .py.tags)) && rm .py.tags && ctags --exclude='*/.git/*'                                                 --languages=python -f .py.tags -R * &)

" Look in the current directory for "tags", and work up the tree towards root until one is found [4]
autocmd FileType shell set tags=./.shell.tags;/
autocmd FileType python set tags=./.py.tags;/
autocmd FileType java set tags=./.java.tags;/


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" EDITING
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" How many spaces to insert it tab hit
set shiftwidth=4

" Move selected lines up and down
nmap <A-k> ddkP
nmap <A-j> ddp
vmap <A-k> dkP`[V`]
vmap <A-j> dp`[V`]

" Flatten a comma separated list
nmap <leader>eff vipJV:s/\s\+/, /g<CR>:noh<CR>
" Unflatten it
nmap <leader>efu :s/,\s*/\r/g<CR>:noh<CR>

" Format in columns
vnoremap <leader>efc :!column -t -s','<CR>

" Format in XML
noremap <leader>efx V:!xmllint --format -<CR>
vnoremap <leader>efx :!xmllint --format -<CR>

" Format in Json
vnoremap <leader>efj :!jq<CR>
noremap <leader>efj vip:!jq<CR>

" comment
vmap <leader>ec <ESC>`<<C-V>`>I
" uncomment
map <leader>eu {j0<C-V>}kElx
vmap <leader>eu <ESC>`<<C-V>`>0Elx

" sort
map <leader>es vip:sort<CR>

" Rename (just in this file)
"nmap <leader>er yiw:%s/<C-R>"/
nmap <leader>er yiw:%s/\([^a-zA-Z0-9-]\\|^\)<C-R>"\([^a-zA-Z0-9-]\)\?/\1\2/gc
vmap <leader>er y:%s/\([^a-zA-Z0-9-]\\|^\)<C-R>"\([^a-zA-Z0-9-]\)\?/\1\2/gc
nmap <leader>er{ yiwvi{:s/\([^a-zA-Z0-9-]\\|^\)<C-R>"\([^a-zA-Z0-9-]\)\?/\1\2/gc
vmap <leader>er{ yvi{:s/\([^a-zA-Z0-9-]\\|^\)<C-R>"\([^a-zA-Z0-9-]\)\?/\1\2/gc

" Copy just this line between the two windows
nmap <leader>iy yy<C-W>pp

" Surround word with any character [11]
nnoremap <leader>s :let c=nr2char(getchar())\|:exec "normal viwo\ei".c."\eea".c."\e"<CR>

" Surround word with curly braces
nmap <leader>i{ wbi{<ESC>Ea}<ESC>
" Surround word with brackets
nmap <leader>i[ wbi[<ESC>Ea]<ESC>
" Surround word with parenthesis
nmap <leader>i( wbi(<ESC>Ea)<ESC>

" Insert template
nmap <C-Space> :read $HOME/Templates/**/*
imap <C-Space> <CR><ESC>k:read $HOME/Templates/**/*


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Snippets
"""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Markdown
"""""""""""
" Underline line with =
nmap <leader>i= yypv$r=
" Underline line with -
nmap <leader>i- yypv$r-
" Insert image
nmap <leader>ii diWa![<C-R>"](<C-R>")<ESC>
" Insert link
nmap <leader>il viW<ESC>Bi[<ESC>Ea]<ESC>yi[Ea(<C-R>")<ESC>T[
vnoremap <leader>il c[<C-R>"](<C-R>")<ESC>T[


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NAVIGATION
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Navigate buffers
nmap <leader>bp :bp<CR>
nmap <leader>bn :bn<CR>
nmap <leader>b# :b#<CR>
nmap <leader>bd :ls<CR>:bd<Space>
nmap <leader>bl :ls<CR>:b<Space>
" Search for tag and open the selected one in a split [8]
"nnoremap <C-w><C-]> <C-w>g]

" navigate clist
nnoremap ]g :cnext<CR>
nnoremap [g :cprev<CR>

" open path under cursor
" just do gf man!

" Move tabs
nnoremap <C-W><C-H> :tabmove -1<CR>
nnoremap <C-W><C-L> :tabmove +1<CR>

" Select last change
nnoremap gp `[v`]

" Jump to next edit point
nnoremap <TAB><Space> <ESC>/<+[^+]*+><Enter>gn
inoremap <TAB><Space> <ESC>/<+[^+]*+><Enter>gn

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SYSTEM INTEGRATION
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Copy the whole file into the system clipboard
nmap <leader>ya gg"+yG''
"
" Copy selection into the system clipboard
vnoremap ys "+y

" Put buffer through pandoc to convert to HTML and copy to clipboard
nmap <leader>yh :w !pandoc -t html \| xclip<CR>
vmap <leader>yh :w !pandoc -t html \| xclip<CR>
"
" Put buffer through pandoc to convert to Jira Markdown and copy to clipboard
nmap <leader>yj :w !pandoc -t jira \| xclip<CR>
vmap <leader>yj :w !pandoc -t jira \| xclip<CR>

" CD into current file's directory
nmap <leader>cd :cd %:p:h<CR>
"nmap <leader>cdR :cd <C-R>=ROOT_PATH<CR><CR>

" Execute command
vnoremap <leader>xs y`>:read !<C-R>"<CR>
nmap <leader>xs yypmm:s/^\$ //e<CR>V\xs'mddk

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" BOOKMARKS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Open config
nmap <leader>oc :tabnew ~/.config/nvim/init.vim<CR>
" Open neovim cheatsheet
nmap <leader>os :tabnew ~/Documents/man/sw/nVIM-cheatsheet.md<CR>

" Open a wiki page
nmap <leader>fw :Files ~/work/doc/wiki<CR>

" Open a script
nmap <leader>fs :Files ~/repos/AXON_utils/<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PLUGINS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDtree [9]
""""""""""""""
nnoremap <leader>nt :NERDTreeToggle<CR>
" Show file in tree
nnoremap <leader>nf :NERDTreeFind<CR>
let g:NERDTreeWinSize=40

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
"autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
"    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FZF search
"""""""""""""
set rtp+=~/repos/fzf/
" Find files
nmap <leader>ff :Files<CR>
" Find buffers
nmap <leader>fb :Buffers<CR>
" Find lines in current buffer
nmap <leader>fl :BLines<CR>
" Find lines in opened buffers
nmap <leader>fL :Lines<CR>
" Find tags in current buffer
nmap <leader>ft :BTags<CR>
" Find tags in the project
nmap <leader>fT :Tags<CR>

" FZFind the word under the cursor
nmap <leader>fw yiw:FZF . <C-R>"<CR>
nmap <leader>fw yiw:Files . -q <C-R>"<CR>
nmap <leader>fW yiW<leader>f -q <C-R>"<CR>

" FZFind highlighted text
vmap <leader>fw y<leader>f -q <C-R>"<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-slime
"""""""""""
let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": "default", "target_pane": "1"}
let g:slime_dont_ask_default = 1


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" AXON SPECIFICS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if $CONTEXTS =~ "AXON"
	let $LD_LIBRARY_PATH="/usr/lib/oracle/12.2/client64/lib:"
	" Execute query and bring results
	nmap <leader>xq yap}pvip:s/%/\\\%/ge<CR>vip:s/!/\\\!/ge<CR>vipd:-1read !sqlturbo.py --format md --user <C-R>=$CUSTOMER_DB<CR> "<C-R>""<CR>
	vmap <leader>xq y'>2o<ESC>Pvip:s/%/\\\%/ge<CR>vip:s/!/\\\!/ge<CR>vipd:-1read !sqlturbo.py --format md --user <C-R>=$CUSTOMER_DB<CR> "<C-R>""<CR>
	
	" no funciona bien:
	" select * from traffic_analysis
	"     where dslam like '%COMBO%'
	"             and port_type='OLT'
	"             and collection_date> trunc(sysdate)-1
	" order by port;
	"
	" no devuelve resultados
	"vnoremap <leader>xq "xymxgvJgvy:read !sqlturbo.py -u <C-R>=$CUSTOMER_DB<CR> "<C-R>""<CR>'xV"xp`>o<ESC>
	" desc(ribe) table or view
	nmap <leader>xd viw<ESC>b<ESC>idesc <ESC>bvee:call slime#send_op(visualmode(), 1)<cr>u
	nmap <leader>xv viwyo<CR>select text from user_views where view_name='<C-R>"';<ESC>o<ESC>kvip:call slime#send_op(visualmode(), 1)<cr>u

	" Execute command in environment
	"nmap <leader>xe yip:read !ssh <C-R>=$CUSTOMER_ENVIRONMENT<CR> '<C-R>"'<CR>
	vnoremap <leader>xe y`>:read !ssh <C-R>=$CUSTOMER_ENVIRONMENT<CR> '<C-R>"'<CR>
	nmap <leader>xe yypmm:s/^\$ //e<CR>V\xe'mddk
	
	"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
	" JIRA
	" Insert SCENARIO divider
	nmap <leader>iS :read $HOME/Templates/markdown/scenario.md<CR>/<++><Enter>"_c4l
	" Insert test
	nmap <leader>iT :read $HOME/Templates/markdown/test.md<CR>/<++><Enter>"_c4l
	" Insert a code block
	nmap <leader>ic :read $HOME/Templates/markdown/code-block.md<CR>/<++><Enter>"_c4l
	vnoremap <leader>ic dk:read $HOME/Templates/markdown/code-block.md<CR>jp?<++><Enter>"_c4l
	" Insert "verified" sentence
	nmap <leader>iv :-1read $HOME/Templates/markdown/verified-in.md<CR>/<++><Enter>"_c4l<C-R>=$CUSTOMER_ENVIRONMENT<ENTER><ESC>n"_c4l<C-R>=$CUSTOMER_VERSION<ENTER><ESC>
	" Insert .unit_test file
	nmap <leader>iu :r .jira/unit_test<CR>
	" Insert _relevant_ sentence
	nmap <leader>ir o<ESC>:read $HOME/Templates/markdown/relevant-files.md<CR>:read !ls *.gz<CR>\iakJ<ESC>
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" LUA
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Now load all the LUA stuff
lua require('basic')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SOURCES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" [1] https://opensource.com/article/18/9/vi-editor-productivity-powerhouse
" [2] https://www.youtube.com/watch?v=XA2WjJbmmoM / https://github.com/changemewtf/no_plugins/
" [3] https://vi.stackexchange.com/questions/17573/function-to-toggle-set-colorcolumn
" [4] https://stackoverflow.com/questions/563616/vim-and-ctags-tips-and-tricks
" [5] https://unix.stackexchange.com/questions/61273/while-in-vi-how-can-i-pull-in-insert-paste-the-contents-of-another-file
" [6] https://shapeshed.com/vim-netrw/
" [7] https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
" [8] https://stackoverflow.com/questions/58347165/vim-tags-how-to-get-c-wc-behave-with-tselect
" [9] https://vimawesome.com/plugin/nerdtree-red
" [10] https://stackoverflow.com/questions/32154285/folding-expanding-and-colapsing-xml-tags-in-vim-xml-parsing
" [11] https://stackoverflow.com/questions/35035787/vim-mapping-to-surround-word-with-a-character
