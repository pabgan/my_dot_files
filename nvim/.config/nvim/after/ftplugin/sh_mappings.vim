autocmd FileType sh setlocal expandtab shiftwidth=4 tabstop=4
" IF snippet
imap if<C-Space> <ESC>ddk:r $HOME/Templates/sh/zsh/if.sh<CR>vip=<TAB><SPACE>
" FOR snippet
imap for<C-Space> <ESC>ddk:r $HOME/Templates/sh/zsh/for.sh<CR>vip=<TAB><SPACE>
" WHILE snippet
imap while<C-Space> <ESC>dd:r $HOME/Templates/sh/zsh/read_file_by_line.sh<CR><TAB><SPACE>
