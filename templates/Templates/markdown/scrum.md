@scrum
======

Attendees
---------

- @ilio_zincone
- @juan_herreria
- @burak_cavdar
- @german_rodriguez
- @ege_sahan
- @miguel_garcia
- @esat_gulkaya
- @iker_almandoz
- @fernando_iglesias
- @kaan_cakmak


To say
------

I have been:

report.sh -p


TODO
----

### me

<+ANYTHING I have to do later, or attend later — whatever tasks and time commitments come up for me during the meeting+>


### other

<+ANYTHING other people promise to do, that my commitments rely on+>


Useful
------

<+Anything that is helpful for me to know to do my job — e.g. budget issues that might impact things I’m doing+>


Questions
---------

<+Any questions that arise in my mind during the meeting (sometimes they are answered during the meeting, sometimes I want to remember to find something out later)+>
