QA
--

### General

- [ES] ¿Cuál dirías que es el **objetivo** del equipo **de QA** en la empresa? ¿Qué valor aporta QA a la empresa?
- [EN] What would you say is the goal of QA in your company?


- [ES] ¿Cuál es la **prioridad** ahora mismo **en QA**?
- [EN] What is currently maximum priority in QA?


- [ES] ¿Cuál es el **reto** a medio y largo plazo **de QA**? ¿Qué medidas se están tomando?
- [EN] What is the challenge of QA in the medium and long run?


- [ES] ¿Qué **pinta** tiene un **Test Plan**? ¿Cómo y cuándo crece? ¿Qué herramienta se utiliza?
- [EN] How does a test plan look like? When and how does it grow? What is the tool you use?


- [ES] Si algún desarrollo toca una parte concreta del software, ¿**cómo** se decide qué pruebas (de **regresión**) se hacen?
- [EN] When there is a development in an area of the software, how do you decide which are the regression tests?


- [ES] ¿Tamaño y organización del equipo?
- [EN] How big is the team? How it is organized?


- [ES] ¿Por qué estáis buscando gente?
- [EN] Why are you looking to hire somebody?


- [ES] ¿Qué os ha interesado de mí?
- [EN] What did it make you want to interview me?


- [ES] ¿Qué otras herramientas se utilizan en el día a día? ¿Jira, GIT...?
- [EN] What other tools do you use in your day to day? Jira, git, teams, word, postman?


### Automation

- [ES] ¿Qué relación tendría yo con _insertar herramientas nombradas en oferta_? ¿Por qué estas herramientas? ¿Se ha valorado utilizar algo como _insertar herramientas alternativas_?
- [EN] What would be my relationship with <++>? Why those tools? Did you consider using <++>?


- [ES] ¿Cómo es un test manual, hay que hacer clicks? ¿Qué porcentaje de tiempo se dedica a **tests manuales**?
- [EN] What does a manual test look like? What percentage of the time would you expect me to put into manual testing?


- [ES] ¿Para qué se utiliza _Shell scripting_?
- [EN] What do you use <++> for?


- [ES] ¿Se usa **Python**? ¿Para qué? ¿Qué futuro tiene?
- [EN] Do you use **Python**? What for? What is its future within the company?



Product
-------

- [ES] ¿Sobre qué **plataforma** se ejecuta?
- [EN] What kind of syste does it run on?


- [ES] ¿Se **personaliza** para los clientes?
- [EN] Is it customized per customer?

	- [ES] ¿Hay QAs asignados a cada cliente?
	- [EN] Are there QA members assigned to customers?


- [ES] ¿Se colabora con algún **proyecto de Software Libre**? Estaría yo involucrado en ello?
- [EN] Do you contribute to any Open Source project? Would I?



Organization
------------

- [ES] ¿Cómo es el **ciclo de desarrollo** que hacéis?
- [EN] How does your development cycle look like?

	- [ES] ¿hay un Scrum Master dedicado a ello?
	 [EN]- Do you have a dedicated Scrum Master?

- [ES] Si yo me pongo a trabajar un lunes y todo el mundo está malo, ¿cómo sé con qué tengo que ponerme?, **¿cómo se organiza el trabajo?**
- [EN] If I wake up on monday and everybody is ill and I finished my tasks at hand on friday, how do I know what to do next? How is the work organized?


- [ES] ¿Qué **coordinación** hay entre **ventas e ingeniería**?
- [EN] What kind of coordination happens between **sales and engineering**?


- [ES] ¿Hay **performance reviews**? ¿Cada cuánto?
- [EN] Is there any kind of **employee performance review**? How often?


- [ES] ¿**Quién** pone los **objetivos**? ¿Qué estilo de objetivos son?
- [EN] Who states my goals? What kind of goals could I expect?


- [ES] ¿**Cómo** voy a ser **evaluado**? En 6 meses, qué haría que pensases que ha merecido la pena contratarme?
- [EN] How am I going to be evaluated? 6 months from now, what would make you think that hiring me was worth while?


- [ES] ¿Tendría **relación** directa con los **clientes**?
- [EN] Would I talk to customers directly?


About the boss
--------------

- [ES] ¿Cómo acabaste en QA? ¿Qué es lo que más te gusta y lo que menos te gusta?
- [EN] How did you end up in QA? What is it that you like most and least from it?


- [ES] ¿Qué es lo que más te gusta de tu empresa y lo que menos te gusta?
- [EN] What is it that you like most and least from your company?


- [ES] ¿La perfección se alcanza cuando no queda nada más que añadir o cuando no se puede quitar nada más?
- [EN] Would you say perfection is achieved when there is nothing else to add or nothing else to strip?


Little things that matter
-------------------------

- [ES] ¿Qué material me daríais para el teletrabajo?, ¿**pantalla**?, ¿**silla**?
- [EN] Would you provide me with work materials? Laptop, screen, chair?


- [ES] ¿Qué **productos de Microsoft** se usan?, ¿Office?, ¿VPN?, ¿Teams?
- [EN] What Microsoft products do you use? Office, VPN, Teams?


- [ES] ¿Qué aplicación de **chat**? ¿Quién es el proveedor de **correo, contactos y calendario**?
- [EN] What is your chat application? What is your mail, contacts and calendar provider?


- [ES] ¿Podría usar linux en mi ordenador?
- [EN] Would I be allowed to use Linux in my computer?

	- [ES] Los usuarios de **Linux**, ¿qué **soporte** reciben por parte de **IT**?
	- [EN] Do **Linux users** users get the same level of support from **IT** than anybody else?

