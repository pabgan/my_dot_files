<+SUBJECT+>
===========


Atendees
--------

- <++>


Summary
-------

<+What was it about, what was said+>


USEFUL
------

<+Anything that is helpful for me to know to do my job — e.g. budget issues that might impact things I’m doing+>


TODO
----

### me

<+ANYTHING I have to do later, or attend later — whatever tasks and time commitments come up for me during the meeting+>


### other

<+ANYTHING other people promise to do, that my commitments rely on+>


QUESTIONS
---------

<+Any questions that arise in my mind during the meeting (sometimes they are answered during the meeting, sometimes I want to remember to find something out later)+>
