<style>
table,th,td {
	border: 1px solid;
}
</style>

<font size="2"/>

| Tipo | Artículo		| Falta | Cantidad ||  Tipo  | Artículo          | Falta | Cantidad |
|--------|----------------------|-------|----------|-|--------|-------------------|-------|----------| 
| Baño   | Papel higiénico	|       |          || Comida | Aceitunas negras	|       |          |
| Baño   | Papel h. húmedo	|       |          || Comida | Pasta fusilli	|       |          |
| Baño   | Pañuelos		|       |          || Comida | Pasta macarrones	|       |          |
| Cocina | Papel horno		|       |          || Comida | Pasta espaguetis	|       |          |
| Cocina | Fairy		|       |          || Comida | Pasta tagliatelle|       |          |
| Cocina | Jabón lavavajillas	|       |          || Comida | Levadura química	|       |          |
| Cocina | Jabón lavadora	|       |          || Comida | Schweppes limón	|       |          |
| Cocina | Papel de cocina	|       |          || Comida | Alioli		|       |          |
| Comida | Leche entera		|       |          || Comida | Mostaza		|       |          |
| Comida | Leche semidesnatada	|       |          || Comida | Mostaza granos	|       |          |
| Comida | Cerveza		|       |          || Comida | Atún bueno	|       |          |
| Comida | Azúcar		|       |          || Comida | Masa quebrada	|       |          |
| Comida | Atún			|       |          || Comida | Mantequilla	|       |          |
| Comida | Queso de untar	|       |          || Comida | Casera		|       |          |
| Comida | Yogur natural	|       |          || Comida | Ginebra		|       |          |
| Comida | Yogur sabores	|       |          || Comida | Tónica		|       |          |
| Comida | Aceite de girasol	|       |          || Comida | Queso provolone	|       |          |
| Comida | AOVE cocinar		|       |          || Comida | Pasta de dientes	|       |          |
| Comida | AOVE aliñar		|       |          || Comida | Queso tetilla	|       |          |
| Comida | Arroz redondo	|       |          || Comida | Harina de trigo	|       |          |
| Comida | Arroz basmati	|       |          || Comida | Queso fresco	|       |          |
| Comida | Cereales		|       |          || Comida | Paté		|       |          |
| Comida | Natillas chocolate	|       |          || Comida | Nocilla		|       |          |
| Comida | Natillas vainilla	|       |          || Comida | Vinagre		|       |          |
| Comida | Queso parmesano	|       |          || Comida | Pacharán		|       |          |
| Comida | Queso grana-padano	|       |          || Comida | Picos		|       |          |
| Comida | Maíz			|       |          || Comida | Tomate pelado entero|    |          |
| Comida | Patatas fritas	|       |          || Comida | Fideos chinos	|       |          |
| Comida | Helados		|       |          || Comida | Vino tinto	|       |          |
| Comida | Galletas		|       |          || Comida | Vino blanco	|       |          |
| Comida | Pan perritos		|       |          || Comida | Jabón manos	|       |          |
| Comida | Queso mozzarella	|       |          || Comida | Salchichas	|       |          |
| Comida | Piñones		|       |          || Comida | Coca-cola	|       |          |
| Comida | Tortitas de maíz	|       |          || ____ | _			|       |          |
| ____ | _			|       |          || ____ | _			|       |          |
| ____ | _			|       |          || ____ | _			|       |          |
| ____ | _			|       |          || ____ | _			|       |          |
| ____ | _			|       |          || ____ | _			|       |          |
