<style>
table { border-collapse: collapse; }
th, td { border: solid black 1px; padding: 0 1ex; }
</style>

Información
-----------

- Dirección: \
             \
             \

- Itinerario: \
              \
              \

- Posibles paradas: \
                    \
                    \
                    \


Preparativos
------------

| Llevar en general | P1 | P2 | P3 | P4 | P5 |
|-------------------|----|----|----|----|----|
| cartera		    |    |    |    |    |    |
| ordenador		    |    |    |    |    |    |
| antimosquitos		|    |    |    |    |    |
| - aparatos		|    |    |    |    |    |
| - pastillas		|    |    |    |    |    |
| pomada picaduras	|    |    |    |    |    |
| cámara de fotos	|    |    |    |    |    |
| trípode		    |    |    |    |    |    |
| Go-Pro		    |    |    |    |    |    |
| medicinas		    |    |    |    |    |    |
| neceseres		    |    |    |    |    |    |
| gafas de sol		|    |    |    |    |    |
| palas ping-pong	|    |    |    |    |    |
| 2a llave coche	|    |    |    |    |    |
| esterillas		|    |    |    |    |    |
| productos herbolario|  |    |    |    |    |
| sábanas  		    |    |    |    |    |    |
| toallas  		    |    |    |    |    |    |
| bolsa de zona     |    |    |    |    |    |
| termo café        |    |    |    |    |    |
| zapas andar casa  |    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |

| Llevar en verano | P1 | P2 | P3 | P4 | P5 |
|------------------|----|----|----|----|----|
| ventilador	   |    |    |    |    |    |
| gorras	       |    |    |    |    |    |
| crema solar	   |    |    |    |    |    |
| bañadores	       |    |    |    |    |    |
| toallas	       |    |    |    |    |    |
| flus-flus	       |    |    |    |    |    |
| Tereré	       |    |    |    |    |    |
| - termo	       |    |    |    |    |    |
| - yerba	       |    |    |    |    |    |
| palas		       |    |    |    |    |    |
| piscinita	       |    |    |    |    |    |
| hamaca	       |    |    |    |    |    |
| antimosquitos    |    |    |    |    |    |
|_		           |    |    |    |    |    |
|_		           |    |    |    |    |    |
|_		           |    |    |    |    |    |

| Llevar para niños	| P1 | P2 | P3 | P4 | P5 |
|-------------------|----|----|----|----|----|
| altavoz bebé      |    |    |    |    |    |
| pañales	        |    |    |    |    |    |
| - tela            |    |    |    |    |    |
| - toallitas	    |    |    |    |    |    |
| - empapadores	    |    |    |    |    |    |
| rhinoflow	        |    |    |    |    |    |
| luz nocturna	    |    |    |    |    |    |
| carro		        |    |    |    |    |    |
| kit pinchazo	    |    |    |    |    |    |
| bolsas vomitar    |    |    |    |    |    |
|_		            |    |    |    |    |    |
|_		            |    |    |    |    |    |
|_		            |    |    |    |    |    |

| Llevar en avión   | P1 | P2 | P3 | P4 | P5 |
|-------------------|----|----|----|----|----|
| pasaportes	    |    |    |    |    |    |
| billetes   		|    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |

| Llevar en camping | P1 | P2 | P3 | P4 | P5 |
|-------------------|----|----|----|----|----|
| hamaca    	    |    |    |    |    |    |
| mesa      		|    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |
|_			        |    |    |    |    |    |

| En casa               | X |
|-----------------------|---|
| Vaciar nevera		    |   |
| Comida para el viaje	|   |
| Comida para la vuelta	|   |
| Coche			        |   |
| - presión		        |   |
| - gasolina	        |   |
| - GPS			        |   |
| Cortar agua		    |   |
| Cortar gas		    |   |
| Bajar persianas	    |   |
| Regar plantas		    |   |
| Apagar enchufes	    |   |
| Encadenar moto	    |   |
| Tirar basura  	    |   |
| Apagar calefacción    |   |
| _			            |   |

Emergencia
----------

- Contactos:
    - Francisco Javier Ganuza Artiles: +34 699 06 95 83
    - María Mercedes Vidal Algarra:    +34 699 07 03 14
    - Javier Moreno:                   +34 620 85 70 55
- Asistencia en carretera:
    - 900 101 399
    - (extranjero) +34 913 93 90 30
    - MGS +34 917 57 24 04
- Cancelar tarjetas
    - ING:       +34 912 06 66 66
    - Revolut:   +34 900 94 32 45
    - Pepephone: +34 634 50 12 12
- Seguro médico:
    - +34 91 111 95 43
    - (extranjero) +34 93 366 95 81


Vuelta
------

| Preparativo		| X |
|-----------------------|---|
| Revisar neceser	|   |
| Actualizar esta lista	|   |

