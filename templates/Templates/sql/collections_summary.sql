SELECT dslam, status_code, COUNT(1) AS line_ids, d.code_description
    FROM dcpc_status_code
    JOIN status_codes_descriptions d
        ON status_code=code_number AND code_type='DcPc'
    WHERE 1=1
        AND collection_date >=TRUNC(sysdate-1)
        AND collection_date<TRUNC(sysdate)
        AND <++>=<++>
GROUP BY dslam, status_code, d.code_description
ORDER BY dslam, status_code ASC;
