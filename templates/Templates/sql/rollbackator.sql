-- FORBIDDEN TABLES:
--   - SELT_DIAGNOSTICS
declare
    ticket varchar(5) := '<++>';
    type array_t is varray(<++>) of varchar2(30);
    OriginTables array_t := array_t('<++>','<++>');
begin
    for i in 1..OriginTables.count loop
        execute immediate('TRUNCATE TABLE '|| OriginTables(i));
        execute immediate('INSERT INTO '||OriginTables(i)||' SELECT * FROM '||OriginTables(i)||ticket);
--        execute immediate('DROP TABLE '||OriginTables(i) || ticket);
    end loop;
end;
exit;
/
