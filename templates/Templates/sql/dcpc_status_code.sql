select o.*, d.code_description
    from dcpc_status_code o
    left join status_codes_descriptions d
        on status_code=code_number
    where 1=1
        and (code_type='DcPc' or code_type is null)
        and collection_date>sysdate-1/24/6
        and <++>='<++>'
order by collection_date;
