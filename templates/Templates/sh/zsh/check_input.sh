help_message="Syntax is: $(basename $0) [--verbose]  <++>"

if [[ $# < <++> ]]; then
	echo "ERROR: invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi
