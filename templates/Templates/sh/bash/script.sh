#!/bin/bash
###############################################################################
## <+What is this all about+>
###############################################################################


###############################################################################
# AUXILIAR
###############################################################################

###############################################################################
# MAIN
###############################################################################

#############################
# 1. PARSE INPUT
help_message="Syntax is: $(basename $0) [-vh] <++>"

while getopts 'vc:h' opt; do
    case "${opt}" in
        (v)
            o_verbose='True'
            ;;

        (c)
            arg="${OPTARG}"
            echo "Processing option 'c' with '${OPTARG}' argument"
            ;;

        (?|h)
            echo "${help_message}"
            exit 1
            ;;
    esac
done
shift "$(($OPTIND -1))"


if (( $# < <++> )); then
    echo ">ERROR> invalid syntax." >&2
    echo "${help_message}" >&2
    exit 2
fi

if [[ ${o_verbose} ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi
