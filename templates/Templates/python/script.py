#!/usr/bin/env python3
import os, argparse

###############################################################################
# AUXILIAR
###############################################################################

def main():
    print("Hello world!")


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    #######################################################
    # Parse arguments
    desc = "<++>.\n"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("--log", help="Select log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)")
    parser.add_argument("<++>")
    args = parser.parse_args()

    if args.log:
        logger.setLevel(args.log)
        logger.debug("Setting log level to {}.".format(args.log))
    else:
        logger.level = logging.WARNING

    main()

